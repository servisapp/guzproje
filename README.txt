ÖĞRENCİ SERVİS WEB UYGULAMASI
Zafer Bilal Gürkan 
Mustafa Yılmaz
2. Öpğretim B Grubu
15.10.2017
GENEL BAKIŞ
1.	Proje Arka Planı ve Açıklaması
		
Üniversitemizde aylık abonelik sistemi ile taşımacılık yapan tur firmaları için;
•	Ödemeleri takip edebilmek.
•	Kayıtlı öğrenciler hakkında detaylı bilgi edinmek.
•	Firma ile ilgili duyuru ve güncellemeleri aktif olarak müşterilere iletmek.
•	Belli sınırlandırmalar ile muhasebe, servis güzergah vb. güncellemeler için farklı yetkiler 
gerektiren bir sistem kurmak.
Sisteme kayıt olan öğrencilerin projeden faydaları;
•	Servis güzergah ve saatleri görebilmek.
•	Ödeme yapmaları gereken tarihler için uyarıları görebilmek.
•	Hesap ayarlayarak kendi bilgilerini yönetebilmek.
2.	Proje Kapsamı
	Bu projenin ana konusu servis taşımacılığı yapan firmaların uygulamalardan bağımsız olarak web üzerinden işlerinin takibini yapabilmeleri sağlamak. Ayrıca öğrenciler için sürekli kullandıkları bu hizmet ile ilgili bilgileri kolay şekilde almaları.
•	Ödemeler ve abonelik tarihleri ile ilgili çıkabilecek aksaklıkları en aza indirme.
•	Personel için daha planlı iş bölümü yaparak kaynakları verimli kullanma.
•	Projenin genişletilebilir yapısı sayesinde tüm tur firmaları için tasarım değişikliği pazarlanabilir olması sayesinde gelir kaynağı oluşturmak.

3.	Üst Düzey Gereklilikler

		
Sistemin ihtiyaçları;
•	Hem iç hem de dış kullanıcılara, uygulamaya herhangi bir yazılım indirmeden erişme olanağı verme yeteneği.
•	HTML5 CSS3 destekleyen güncel bir web görüntüleyicisi.
•	Sakarya üniversitesi öğrenci numarasına sahip olmak.
	

